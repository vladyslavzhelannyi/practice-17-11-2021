package main.java.com.practice.tasks;

import java.util.Scanner;

public class PracticeTasks {
    private static final String INCORRECT_INPUT = "Ввод некорректный";
    private static final String PLUS = "+";
    private static final String MINUS = "-";
    private static final String DIVIDE = "/";
    private static final String MULTIPLY = "*";
    private static final String REMAINDER_OF_DIVISION = "%";
    private static final String RESULT = "Result: ";
    private static final String EXIT = "exit";
    private static final String ZERO = "0";
    private static final String ONE = "1";
    private static final String TWO = "2";
    private static final String THREE = "3";
    private static final String FOUR = "4";
    private static final String FIVE = "5";
    private static final String SIX = "6";
    private static final String SEVEN = "7";
    private static final String EIGHT = "8";
    private static final String NINE = "9";


    public String countEvenAndUnevenDigits(int number){
        int digit;
        int evenNumbersSum = 0;
        int unevenNumbersSum = 0;
        String result;
        if(number == 0){
            evenNumbersSum = 1;
        }
        else{
            while(number != 0){
                digit = number % 10;
                if(digit % 2 == 0){
                    evenNumbersSum++;
                }
                else{
                    unevenNumbersSum++;
                }
                number /= 10;
            }
        }
        result = "В данном числе:\nчетных цифр - " + evenNumbersSum + ";\nнечетных цифр - " + unevenNumbersSum + ".";
        return result;
    }

    public void consoleCalculator(Scanner scanner){
        while(true){
            int firstNumber = 0;
            int secondNumber = 0;
            int result;
            String arithmeticOperator;
            System.out.println("Введите выражение, которое хотите вычислить(+,-,/,*,%): ");
            String userInput = scanner.nextLine();
            if(userInput.equalsIgnoreCase(EXIT)){
                exit();
            }
            String[] userInputParts = expressionValidator(userInput);
            if(userInputParts[0].equals(INCORRECT_INPUT)){
                System.out.println(INCORRECT_INPUT);
                continue;
            }
            if(userInputParts.length != 3){
                System.out.println(INCORRECT_INPUT);
                continue;
            }
            firstNumber = Integer.valueOf(userInputParts[0]);
            secondNumber = Integer.valueOf(userInputParts[2]);
            arithmeticOperator = userInputParts[1];
            switch(arithmeticOperator){
                case PLUS:{
                    result = firstNumber + secondNumber;
                    System.out.println(RESULT + result);
                    break;
                }
                case MINUS:{
                    result = firstNumber - secondNumber;
                    System.out.println(RESULT + result);
                    break;
                }
                case DIVIDE:{
                    if (secondNumber == 0){
                        System.out.println("Деление на ноль невозможно");
                    }
                    else {
                        result = firstNumber / secondNumber;
                        System.out.println(RESULT + result);
                    }
                    break;
                }
                case MULTIPLY:{
                    result = firstNumber * secondNumber;
                    System.out.println(RESULT + result);
                    break;
                }
                case REMAINDER_OF_DIVISION:{
                    result = firstNumber % secondNumber;
                    System.out.println(RESULT + result);
                    break;
                }
                default:{
                    System.out.println(INCORRECT_INPUT);
                }
            }
        }
    }

    private String[] expressionValidator(String userInput){
        String[] stringExpressionParts;
        String[] allowableValues = {ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, PLUS, MINUS, DIVIDE, REMAINDER_OF_DIVISION};
        

        return stringExpressionParts;
    }

    private String[] deleteElement(String[] array, int index){
        String[] tempArray =
    }

    public String bitwiseOr(String firstBitNumber, String secondBitNumber){
        String[] firstNumberBits = firstBitNumber.split("");
        String[] secondNumberBits = secondBitNumber.split("");
        String resultDigit;
        String result = "";
        int numberLength = firstNumberBits.length;
        for(int i = 0; i < numberLength; i++){
            if(firstNumberBits[i].equals(ZERO) & secondNumberBits[i].equals(ZERO)){
                resultDigit = ZERO;
            }
            else{
                resultDigit = ONE;
            }
            result += resultDigit;
        }
        return result;
    }

    private void exit(){
        System.exit(0);
    }

//    public

}
